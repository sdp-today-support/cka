# CKA weblinks

* [Official CKA webpage](https://www.cncf.io/training/certification/cka/)
* PSI and Killer
  * [Important Instructions: CKA and CKAD](https://docs.linuxfoundation.org/tc-docs/certification/tips-cka-and-ckad)
  * [CKS CKA CKAD changed Terminal to Remote Desktop](https://itnext.io/cks-cka-ckad-changed-terminal-to-remote-desktop-157a26c1d5e)
  * [Update on Certification Exam Proctoring Migration](https://training.linuxfoundation.org/blog/update-on-certification-exam-proctoring-migration/?utm_source=lftraining&utm_medium=twitter&utm_campaign=blog)
  * [Linux Foundation Proctoring Platform – Migrating to PSI Bridge](https://training.linuxfoundation.org/bridge-migration-2021/)
  * [PSI Online Proctoring Experience](https://psi.wistia.com/medias/5kidxdd0ry) (video)
  * [Taking the Exam](https://docs.linuxfoundation.org/tc-docs/certification/lf-handbook2/taking-the-exam)
  * [Linux Foundation Exam Simulators](https://killer.sh/faq)
  * [CKS CKA CKAD LFCS LFCT Simulator](https://killer.sh/)
  * [CKS CKA CKAD Remote Desktop](https://killercoda.com/kimwuestkamp/scenario/cks-cka-ckad-remote-desktop)
* [Frequently Asked Questions: CKA and CKAD & CKS](https://docs.linuxfoundation.org/tc-docs/certification/faq-cka-ckad-cks)
* [LF training portal](https://trainingportal.linuxfoundation.org/learn/dashboard)
* [curriculum](https://github.com/cncf/curriculum)
* Webpages
  * [CKAD/CKA 證照考試心得](https://fullstackladder.dev/blog/2023/07/29/how-to-pass-kubernetes-ckad-cka-certificate/) (2023-07-29)
  * [CKA 考試一週準備心得](https://stringpiggy.hpd.io/cka-certified-kubernetes-administrator-%E4%B8%80%E9%80%B1%E6%BA%96%E5%82%99%E5%BF%83%E5%BE%97/) (2022-11-27)
  * [K8S CKA 考試總結](https://isaacyip.com/blog/k8s_cka_exam/) (2022-12-10)
  * [(CKA) 證照心得](https://j-sui.com/2021/12/24/cncf-cka-experience/) (2021-12-24)
  * [我是如何通過 CKA](https://easoncao.com/cka-cncf-certified-kubernetes-administrator-study-guide/) (2021-11-15)
  * [【後記】CKA證照](https://ithelp.ithome.com.tw/articles/10253064) (2020-10-14)
* Learning materials 
  * udemy
    * [Kubernetes for the Absolute Beginners - Hands-on](https://www.udemy.com/course/learn-kubernetes/)
    * [Certified Kubernetes Administrator (CKA) with Practice Tests](https://www.udemy.com/course/certified-kubernetes-administrator-with-practice-tests/)
  * [Kubernetes Certification Tips](https://www.youtube.com/watch?v=wgfjXHw7uPs) (2018-12-20)
    1. Attempt all Questions
    2. Don't get Stuck!
    3. Get good with YAML
    4. Use Shortcuts/Aliases
  * [CKA-Exercises](https://github.com/chadmcrowell/CKA-Exercises/tree/main)
    * [kubectl Cheat Sheet](https://kubernetes.io/docs/reference/kubectl/cheatsheet/)
    * killercoda.com
      * [Chad M. Crowell](https://killercoda.com/chadmcrowell)
      * [CKA](https://killercoda.com/killer-shell-cka)
  * [K8s-bookmarks-CKA](K8s-bookmarks-CKA.html) (可在 chrome 匯入書籤)
* [在臺有戶籍國民在國內申請換發普通護照說明書](https://www.boca.gov.tw/cp-300-7141-f30e2-1.html)
* [Webcam](https://www.tk3c.com/pt.aspx?pid=218018&p=1&kw=webcam&ec=Search_goodsPC1)
